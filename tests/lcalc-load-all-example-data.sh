#!/bin/sh
#
# Ensure that "lcalc -F" can load all of the example data we ship.
#

printf "Testing lcalc -F in doc/examples/data... "

if find "${example_data}" -type f \
                          -name 'data_*' \
                          -exec "${lcalc}" -F '{}' \; ; then
  printf "PASS\n"
  exit 0
else
  printf "FAIL\n"
  exit 1
fi
