2021-11-25      Michael Orlitzky <michael@orlitzky.com>

	* === Released 2.0.4 ===
	* Correct a few mistakes in the manpage (Julien Puydt).
	* Fix the build with newer clang and non-GNU libc++.
	* Happy Thanksgiving.

2021-07-13	Michael Orlitzky <michael@orlitzky.com>

	* === Released 2.0.3 ===
	* Revert libLfunction's soname to libLfunction.so.1.
	* Don't show --help output for flags that are disabled.
	* Fix a warning in C++17 mode.
	* Remove a bunch of dead code.

2021-07-05	Michael Orlitzky <michael@orlitzky.com>

	* === Released 2.0.2 ===
	* Fix compilation with -std=c++11.

2021-06-25	Michael Orlitzky <michael@orlitzky.com>

	* === Released 2.0.1 ===
	* Fix a deadly typo in configure.ac.

2021-06-22	Michael Orlitzky <michael@orlitzky.com>

	* === Released 2.0.0 ===
	* Use explicit formulas to verify zeros and make errors less likely
	* Fixed a subtle bug wherein one zero was counted with multiplicity,
	  and a corresponding number of subsequent zeros were then overlooked
	* New "-N" flag for lcalc to start searching from the Nth zero
	* Improved derivative routines
	* Improved output precision
	* Implemented custom trigonometric and complex exponential functions
	  for faster computations
	* Upstream development moved to https://gitlab.com/sagemath/lcalc
	* New autotools build system
	* The libLfunction library is now libtool-versioned
	* Support modern versions of PARI
	* Compile with newer versions of GCC and Clang
	* Headers officially installed to "lcalc" directory
	* We now install a pkg-config file
	* Automated "make check" test suite
	* New lcalc(1) man page
	* Removed the unnecessary --url (-u) option. Users are encouraged
	  to run wget (or curl, or whatever) themselves to retrieve URLs.
